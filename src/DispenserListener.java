package me.matt.tntthrottle;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

public class DispenserListener implements Listener {

	private TNT plugin;

	public DispenserListener(TNT tnt) {
		this.plugin = tnt;
	}

	public List<Location> throttle = new ArrayList<Location>();

	@EventHandler
	public void onDispense(BlockDispenseEvent event) {
		Block block = event.getBlock();
		Location location = block.getLocation();
		ItemStack item = event.getItem();

		if (block.getType().equals(Material.DISPENSER) && item.getType().equals(Material.TNT)) {
			if (throttle.contains(location)) {
				event.setCancelled(true);
			} else {
				throttle.add(location);

				Integer delay = plugin.getConfig().getInt("timing.delay");

				BukkitTask task = new Lockout(plugin, this, location, delay).runTaskTimer(plugin, 5L, 1L);
			}
		}
	}

}
