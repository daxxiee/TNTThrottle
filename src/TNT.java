package me.matt.tntthrottle;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class TNT extends JavaPlugin {

	// Fired when plugin is first enabled
	@Override
	public void onEnable() {
		createConfig();
		getServer().getPluginManager().registerEvents(new DispenserListener(this), this);
	}

	// Fired when plugin is disabled
	@Override
	public void onDisable() {

	}

	private void createConfig() {
		try {
			if (!getDataFolder().exists()) {
				getDataFolder().mkdirs();
			}
			File file = new File(getDataFolder(), "config.yml");
			if (!file.exists()) {
				getLogger().info("Config.yml not found, creating!");
				saveDefaultConfig();
			} else {
				getLogger().info("Config.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String configColor(String line) {
		Map<String, String> values = new HashMap<String, String>();
		return configColor(line, values);
	}

	public String configColor(String line, Map<String, String> values) {
		String color = ChatColor.translateAlternateColorCodes('&', line);

		StrSubstitutor sub = new StrSubstitutor(values, "$(", ")", '\\');
		String result = sub.replace(color);
		return result;
	}

}
