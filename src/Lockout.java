package me.matt.tntthrottle;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import de.inventivegames.hologram.Hologram;
import de.inventivegames.hologram.HologramAPI;

public class Lockout extends BukkitRunnable {

	private TNT plugin;
	private DispenserListener DL;
	private Location location;
	private Integer delay;
	private Hologram hologram;

	public Lockout(TNT plugin, DispenserListener DL, Location location, Integer delay) {
		this.plugin = plugin;
		this.DL = DL;
		this.location = location;
		this.delay = delay;
		this.hologram = HologramAPI.createHologram(location.clone().add(0.5, 2, 0.5), "");
		update();
		hologram.spawn();
	}

	@Override
	public void run() {
		update();
		if (--this.delay < 0) {
			this.cancel();
			hologram.despawn();
			DL.throttle.remove(this.location);
		}

	}

	private void update() {
		Map<String, String> values = new HashMap<String, String>();
		
		NumberFormat formatter = new DecimalFormat("#0.00");     
		String format = formatter.format((float)this.delay / 20);
		
		values.put("delay", format);
		String name = plugin.configColor(plugin.getConfig().getString("hologram.delay"), values);

		hologram.setText(name);
	}

}
